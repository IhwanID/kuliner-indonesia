class Food {
  String name;
  String image;
  String description;

  Food({
    this.name,
    this.image,
    this.description,
  });

  factory Food.fromJson(Map<String, dynamic> parsedJson) {
    return Food(
        name: parsedJson['name'],
        image: parsedJson['image'],
        description: parsedJson['description']);
  }
}
