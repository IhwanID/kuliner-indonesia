import 'package:flutter/material.dart';
import 'detail.dart';
import 'package:kuliner_indonesia/model/food.dart';
import 'dart:convert';
import 'package:kuliner_indonesia/animation/hero.dart';

class DessertScreen extends StatefulWidget {
  @override
  _DessertScreenState createState() => _DessertScreenState();
}

class _DessertScreenState extends State<DessertScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getFoodList(),
    );
  }

  getFoodList() {
    return Container(
      child: Center(
        child: FutureBuilder(
            future:
                DefaultAssetBundle.of(context).loadString('data/dessert.json'),
            builder: (ctx, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data != null) {
                  List<Food> food = parseJson(snapshot.data);
                  return food.isNotEmpty
                      ? _showFoodList(context, food)
                      : Center(child: Text("No Food List Found.."));
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }),
      ),
    );
  }

  Widget _showFoodList(BuildContext context, List<Food> data) =>
      GridView.builder(
        itemCount: data == null ? 0 : data.length,
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            child: Card(
              elevation: 5.0,
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              margin: EdgeInsets.all(10),
              child: GridTile(
                child: PhotoHero(
                  tag: data[index].name,
                  onTap: () {
                    Navigator.push(
                        context,
                        PageRouteBuilder(
                          transitionDuration: Duration(milliseconds: 777),
                          pageBuilder: (BuildContext context,
                                  Animation<double> animation,
                                  Animation<double> secondaryAnimation) =>
                              DetailScreen(
                            name: data[index].name,
                            image: data[index].image,
                            description: data[index].description,
                          ),
                        ));
                  },
                  photo: "images/${data[index].image}",
                ),
              ),
            ),
          );
        },
      );

  List<Food> parseJson(String response) {
    if (response == null) {
      return [];
    } else {
      final parsed =
          json.decode(response.toString()).cast<Map<String, dynamic>>();
      return parsed.map<Food>((json) => Food.fromJson(json)).toList();
    }
  }
}
