import 'package:flutter/material.dart';

class DetailScreen extends StatelessWidget {
  DetailScreen({this.name, this.image, this.description});

  final String name, image, description;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('Detail Makanan'),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false),
            )),
        body: getDetailFood());
  }

  getDetailFood() {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Hero(
              tag: name,
              child: Image.asset('images/$image'),
            ),
            Center(
                child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                name,
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
              ),
            )),
            Container(
              padding: EdgeInsets.all(4.0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Deskripsi :",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      description,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
